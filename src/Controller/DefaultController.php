<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * TODO: Add the double asterisk in the line above for this route to work
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
        $time_start = microtime(true);

        for ($i=0; $i<1000; $i++) {
            $file = fopen(__DIR__, "r");
        }

        $time_end = microtime(true);
        $time = $time_end - $time_start;

        print("<p>File system IO speed: " . $time . " s.</p>");



        $time_start = microtime(true);

        for ($i=0; $i<3; $i++) {
            $file = file_get_contents("http://www.google.com");
        }

        $time_end = microtime(true);
        $time = $time_end - $time_start;

        print("<p>Network speed: " . $time . " s.</p>");


        $time_start = microtime(true);

        for ($i=0; $i<10; $i++) {
            $a = array_fill(0, 1000, mt_rand(0, 1000));
            $b = array_fill(0, 1000, mt_rand(0, 1000));
            $c = [];
            foreach ($a as $h) {
                foreach ($b as $j) {
                    $c[] = $h*$j;
                }
            }
        }

        $time_end = microtime(true);
        $time = $time_end - $time_start;

        print("<p>PHP execution speed: " . $time . " s.</p>");


        return new Response( "<p>hello world</p>");
    }

    public function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }



}
